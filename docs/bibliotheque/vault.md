# Vault

Dans cette aventure, vous allez avoir besoin de renforcer la sécurité votre `sith`.

Pour cela, vous pourrez soit utiliser :

- le gourdin [BanzaiCloud Bank Vault](https://github.com/banzaicloud/bank-vaults) qui vous permettra de distribuer de façon sécurisée vos différents secrets via un opérateur qui par mutation enrichira vos secrets avec vault.
- la fronde [Vault CSI Provider](https://developer.hashicorp.com/vault/docs/platform/k8s/csi) qui en s'appuyant sur les [CSI Secrets Store](https://secrets-store-csi-driver.sigs.k8s.io/) de kubernetes va permettre de créer des "SecretProviderClass" qui seront utilisés en replacement des secrets classiques.

Mais si vous avez déjà lu cela, sachez que

> The password is `GH{ApP3xTNhNzBTV0RiMwVXR2cwXxY6NggXBlYdS01AEQcPZ2ESW1Y7MQECXwEJVwMCDww=}`

## Utiliser la cmdline vault

Avec notre installation de vault dans kubernetes

### Avec le vault operator

```sh
export VAULT_CACERT=/vault/tls/ca.crt
export VAULT_TOKEN=$(kubectl -n vault-operator get secrets vault-unseal-keys -o jsonpath={.data.vault-root} | base64 --decode)

# command "vault help"
kubectl -n vault-operator exec vault-0 -c vault -- /bin/sh -c "VAULT_TOKEN=$VAULT_TOKEN VAULT_CACERT=/vault/tls/ca.crt vault help"

```

### Avec le vault csi

```sh
# command "vault help"
kubectl -n vault exec vault-0 -- /bin/sh -c "VAULT_TOKEN=$VAULT_TOKEN VAULT_CACERT=/vault/tls/ca.crt vault help"

```

## Ajouter d'un secret dans vault

```sh
# créer l'entrée secret/my-super-secret et mettre une clef/valeur password=mon-password-dans-vault (une entrée peut avoir plusieurs k/v)
vault kv put secret/my-super-secret password=mon-password-dans-vault
```

Pour plus d'information sur les commandes avec la cli vault concernant la gestion des secrets https://developer.hashicorp.com/vault/docs/commands/kv
