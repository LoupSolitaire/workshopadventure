# DHALL-LANG

Dhall-lang, le langage de configuration fonctionnel https://dhall-lang.org/

## Emplacement du cache

Dhall télécharge et mets en cache les différents types réutilisables, dont ceux de kubernetes. Par défaut ce cache est dans le répertoire `~/.cache/dhall`. Les premiers build avec un cache vide peuvent prendre plusieurs minutes puis quelques seconde une fois les éléments mis en cache.

## Binaires

Le format de dhall est un format "json-like" qui permet de générer des fichiers de configuration dans différents formats. Pour chaque format, il y a binaire dédié :

- json : dhall-to-json
- yaml : dhall-to-yaml
- toml : dhall-to-toml
  ...

On peut trouver ces binaires : https://github.com/dhall-lang/dhall-haskell/releases

Et il y a aussi des binaire permettant à partir d'un format de configuration json, yaml, ... de générer un format dhall mais non typé. Il est aussi possible à partir d'un endpoint de type openapi de générer les types au format dhall des objets définis, c'est ce qui est fait pour générer le "binding" dhall pour kubernetes : https://github.com/dhall-lang/dhall-kubernetes

## Commandes de base

Les options essentielles à connaîtres sont :

le binaire `dhall` renvoie le typage au format dhall

Options communes pour `dhall`, `dhall-to-yaml`, `dhall-to-json`, ...

- `--file nom_du_fichier.dhall` : prend en entrée le fichier .dhall
- `--explain` : donne des informations sur l'erreur ou si pas d'erreur sur le typage de la sortie dhall
  par defaut la sortie est dans la console, l'option `--output` permet d'écrire le résultat dans un fichier.

Pour `dhall-to-yaml`

- `--documents` : ajoute un séparateur entre les documents yaml (`---`)

La liste des options de chaque binaire est accessible via l'option `--help`
