### Helm 🚒 dans les runner GitLab

Vous allez intégrer dans CI/CD les instructions pour déployer du helm en utilisant les manifests générés par helm (au lieu de la commande d'installation que fournit aussi helm) et en spécifiant le fichier de valeurs à utiliser en fonction de l'environnement à déployer.

L'équivalent de la commande précente (pour déployer le chart helm qui est dans le dossier `enfer`) donnerait dans le fichier .gitlab-ci.yaml

```yaml
# ...

helm-dev:
  stage: deploy-helm-exemple
  extends: .kubeconfig
  script:
    - echo "👋 Deploy helm dans le namespace helm-example"
    - kubectl create ns helm-example 2>/dev/null || true
    - helm template example --create-namespace -n helm-example -f ./enfer/values-example.yaml --set testConnection=false ./enfer | kubectl apply -n helm-exemple -f -
# ...
```

**Depuis votre workspace gipods (ou gitlab avec le Web IDE intégré) `deploy-sith-from-gitlab`.**

À vous :

- d'ajouter dans le fichier `.gitlab-ci.yaml` le job permettrant de déployer la version de **production**
- et de modifier la configuration de production pour utiliser la version **1.0** du `sith`.

**TIPS :** Vous trouverez dans le fichier `values.yaml` l'url du sith
