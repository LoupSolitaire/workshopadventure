## Intégration de Vault via les "CSI"

Dans cette aventure, vous allez avoir besoin de renforcer la sécurité votre `sith`. Pour cela, vous avez choisi d'utiliser [Vault CSI Provider](https://developer.hashicorp.com/vault/docs/platform/k8s/csi) qui vous permet d'utiliser "secrets-store" alimenté par vault en replacement des secrets classiques.

Pour cela vous allez :

- installer vault et le csi provider (via un script)
- l'utiliser avec un nouveau type d'objet vous permettant d'accéder aux secrets

Vous trouverez par ailleurs les différents type de deploiements (kustomize, helm, dhall).

Ces descripteurs vous permettront de :

- récupérer dans l'entrée du serveur vault "secret/james-secret" la valeur ayant pour clef "secret" (via un SecretProviderClass)
- monter ce secret en tant que fichier "/etc/sith/secret" via le driver CSI dans votre deployement

### Installer vault et son CSI provider

**Depuis votre livre de sorts (répertoire `/workspace/spell-book/`).**  
Il faut que vous puissiez passer des commandes kubectl.

👀 Chapitre (aka sous dossier) `secrets/vault-csi`

Installer vault et le provider CSI

```shell
./install_and_configure_vault_csi.sh
```

Ce script va :

- créer un namespace pour vault et le csi provider.
- installer et preconfigurer vault dans le cluster (ne faites pas cela en production).
- installer le kubernetes secret store CSI driver puis le vault CSI provider (l'implémentation pour vault, on peut utiliser d'autres kms)
- créer une entrée vault `secret/james-secret` contenant la k/v `secret="👋 Hello from Vault CSI !"`

Avec ce CSI, l'injection de secret dans un manifest de pod se fait en utilisant un objet de type `SecretProviderClass`. Cela permettra de le monter dans le pod en tant que volume csi des secrets stoqués dans Vault.

### Utiliser vault et son CSI provider pour déployer notre sith

Vous utiliserez les différents manifest déjà présent en les intégrants soit dans votre pipeline GitLab, soit dans ArgoCD.  
Comme précédemment ces manifests sont dans le repo gitlab et suivent la même logique que les autres déploiements (un fichier de valeurs ou un dossier d'overlays ayant un pattern `vault-csi` à utiliser dans votre processus de déploiement)

- La version du sith à déployer est la version `1.3`
- Le namespace qui doit être utilisé est `[dhall|helm|kustomize]-vault-csi-secret` (selon que choisissiez dhall ou helm ou kustomize)
- La struture et les fichiers pour utiliser vault sont disponibles mais il faudra les compléter pour qu'ils soient fonctionnels.

### Comment injecter un secret vault dans un secret kubernetes

Pour mériter le titre de gardien des secrets vault vous allez devoir en plus de configurer l'integration GitLab ou ArgoCD, finir de configurer les parties spécifiques à vault.

Pour créer un secret dans vault, vous pouvez par exemple utiliser l'outil en ligne de commande vault qui est disponible dans le pod de Vault. Pour cette exercice ce ne sera pas nécessaire, le secret est déjà créé dans la phase initiale. La commande suivante est données à titre d'exemple.

```sh
## création de l'entrée dans le vault déployé dans le cluster
kubectl -n vault exec vault-0 -- /bin/sh -c "vault kv put secret/my-secret password=mon-password-dans-vault"
```

Pour cette épreuve vous allez devoir intégrer le secret vault dans l'entrée `secret/james-secret` contenant la k/v `secret="👋 Hello from Vault CSI !"`. Ce secret devra être monté dans vos pods (dans le dossier /etc/sith), dans un fichier appelé `secret`.

Pour accéder à ce secret et le rendre disponible dans un cluster l'on utilise un SecretProviderClass. Les descripteurs suivants sont donnés à titre d'exemple, regardez dans les configurations (dhall ou helm ou kustomize en fonction de ce que vous avez choisi précédemment). Les valeurs à remplacer sont identifiées via des "FIXME"

```yaml
## Example d'un "SecretProviderClass" d'identifiant "my-secret-store" permettant de récupérer le contenu du secret suivant :
# - hébergé sur le serveur vault accessible dans le cluster à l'adresse "http://vault.vault:8200"
# - dans l'entrée "secret/my-secret"
# - ayant la clef "password"
# Le contenu sera monté (dans le pod l'utilisant) en tant que fichier de nom "dbPassword"
---
apiVersion: secrets-store.csi.x-k8s.io/v1
kind: SecretProviderClass
metadata:
  name: my-secret-store
spec:
  provider: vault
  parameters:
    roleName: 'csi'
    vaultAddress: 'http://vault.vault:8200'
    objects: |
      - objectName: "dbPassword"
        secretPath: "secret/data/my-secret"
        secretKey: "password"
```

**Note**: d'autres options et configurations permettent de faire pont entre ce type d'objet et les secrets kubernetes ([pour plus d'information](https://secrets-store-csi-driver.sigs.k8s.io/topics/sync-as-kubernetes-secret.html))

On peut ensuite monter ce secret vault en tant que fichier (ici de nom dbPassword) dans un pod. Pour cela nous allons le monter en tant que volume CSI.  
Dans l'exemple ce pod monte le SecretProviderClass "my-secret-store" :

- dans un fichier "/etc/secrets/dbPassword" (dbPassord étant l'"objectName" précédement définie)
- via le volume csi de driver "secrets-store.csi.k8s.io"

```yaml
---
kind: Pod
apiVersion: apps/v1
metadata:
  name: secure-pod
spec:
...
containers:
  - ...
    volumeMounts:
    - mountPath: /etc/secrets
      name: secret-volume
  volumes:
    - name: secret-volume
      csi:
        driver: secrets-store.csi.k8s.io
        readOnly: true
        volumeAttributes:
          secretProviderClass: "my-secret-store"
...
```
