#!/bin/sh
# from https://www.hashicorp.com/blog/retrieve-hashicorp-vault-secrets-with-kubernetes-csi

# Enable kube auth
echo "🔧 Enable and configure kube authentification"
set -x
vault auth enable kubernetes
vault write auth/kubernetes/config \
    kubernetes_host="https://kubernetes.default:443"

# Enable access to secrets for pods
{ set +x; } 2> /dev/null # silently disable xtrace
echo "🔧 Create policy can read all secrets (don't do this)"
set -x
# create policy for access to all secrets in read only (don't do this)
vault policy write csi - <<EOF
path "secret/data/*" {
    capabilities = ["read"]
}
EOF

{ set +x; } 2> /dev/null # silently disable xtrace
vault policy read csi

echo "🔧 Create named role for all sa and all ns with the reading policy (don't do this)"
set -x
vault write auth/kubernetes/role/csi \
   bound_service_account_names="*" \
   bound_service_account_namespaces="*" \
   policies=csi \
   ttl=120m

# Create the KV pair secret "james secret"
{ set +x; } 2> /dev/null # silently disable xtrace
echo "🕵️ Add james secret for test"
set -x
vault kv put secret/james-secret secret="👋 Hello from Vault CSI !"