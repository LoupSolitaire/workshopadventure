#!/bin/bash

## 
set -e
DIR=$(dirname "$0")

echo "🏗️ Removing if necessary BanzaiCloud Vault-Operator"
kubectl delete ns vault-operator 2> /dev/null || true
kubectl delete secret -A -l vault_cr=vault 2> /dev/null || true

set -x

echo "🏗️ Installing BanzaiCloud Vault-Operator"
kubectl create ns vault-operator
kubectl label namespace vault-operator name=vault-operator
kubectl apply -n vault-operator -f vault-operator
kubectl apply -n vault-operator -f rbac.yaml

set +x
echo "🔗  Wait for CRD to be accounted by api-server"
sleep 5

set -x
kubectl apply -n vault-operator -f cr.yaml
kubectl apply -n vault-operator -f vault-secrets-webhook

set +x
echo "🔗 Waiting until available"
until kubectl -n vault-operator get pod -l app.kubernetes.io/name="vault" -o go-template='{{.items | len}}' | grep -qxF 1; do
    echo -n "."
    sleep 2
done
echo
kubectl wait pod vault-0 -n vault-operator --timeout=-1s --for=jsonpath='{.status.phase}'=Running

echo "🔗 Waiting for vault to be operational"
sleep 20

set -x
export VAULT_CACERT=/vault/tls/ca.crt
export VAULT_TOKEN=$(kubectl -n vault-operator get secrets vault-unseal-keys -o jsonpath={.data.vault-root} | base64 --decode)
sleep 1

echo "🔗 Creating james-secret"
kubectl -n vault-operator exec vault-0 -c vault -- /bin/sh -c "VAULT_TOKEN=$VAULT_TOKEN VAULT_CACERT=/vault/tls/ca.crt vault kv put secret/james-secret content='👋 Hello from Vault Operator :)'"
